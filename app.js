// create a server
var Hapi = require('hapi');
var mongoose = require('mongoose');
var server = new Hapi.Server();
var Path = require('path');
var Joi = require('joi');
var controllers = require('./controllers');
var HapiSwagger     = require('hapi-swagger');
var pack            = require('./package');
server.connection({ port: 3002 });

var swaggerOptions = {
    apiVersion: pack.version
};

server.route({
    method: 'GET',
    path: '/api/car/carList',
    config: {
        cors: true,
        description: 'Fetch car Data',
        tags: ['api', 'car'],
        handler: function (request, reply) {

            var Criteria = {};
            if(request.query.flag === 0){ }
            if(request.query.flag === 1){ Criteria = {year : request.query.data} }
            if(request.query.flag === 2){ Criteria = {make : request.query.data} }
            if(request.query.flag === 3){ Criteria = {price : request.query.data} }

            controllers.car.fetchCarList(Criteria ,function(error, success) {
                if (error) {
                    reply(error.response).code(error.statusCode);
                } else {
                    reply(success.response).code(success.statusCode);
                }
            });
        },
        validate: {
            query: {
                data: Joi.string().trim(),
                flag: Joi.number()    //0 = blank, 1 = year, 2 = make, 3= price
            }
        },
        response : {
            options: {
                allowUnknown: true
            },
            schema: {
                message: Joi.string(),
                data: Joi.array().required().items(
                    Joi.object().keys({
                        carID : Joi.any(),
                        name : Joi.any(),
                        price :Joi.any(),
                        make : Joi.any(),
                        model : Joi.any(),
                        year : Joi.any(),
                        imageUrl : Joi.any()
                    })
                )
            }
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: [
                    {code: 200, message: 'OK'},
                    {code: 411, message: 'Car Not Found'},
                    {code: 500, message: 'Internal Server Error'}
                ]
            }
        }
    }
});

server.register([
    {
        register: HapiSwagger,
        options: swaggerOptions
    }], function (err) {
    server.start(function(){
        console.log('Server running at:', server.info.uri);
    });
});


mongoose.connect('mongodb://localhost/car', function(err, db) {
    if(!err) {
        console.log("We are connected");
    }
});



