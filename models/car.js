
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var car = new Schema({
    name : {type: String, unique: true, required: true},
    make: {type: String, unique: true, required: true},
    carImage: {type: String, default: ''},
    price : {type : Number, default : 0},
    year : {type : String, required: true}
});

module.exports = mongoose.model('car', car);