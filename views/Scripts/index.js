$(document).ready(function () {
    getAllVehicleService('empty', 0);
});

$('input').keypress(function (e) {
    if (e.which == 13) {
        if (this.id == "year") {
            var flagVal = 1;
            var searchData = $('#year').val();
        } else if (this.id == "price") {
            flagVal = 2;
            var searchData = $('#price').val();
        } else if (this.id == "make") {
            flagVal = 3;
            var searchData = $('#make').val();
        }
        getAllVehicleService(searchData, flagVal);
    }
});

getAllVehicleService = function (searchData, flagVal) {

    $.ajax({
        url: "http://localhost:3002/api/car/carList",
        type: "GET",
        data: {data: searchData, flag: flagVal},
        dataType: "json",
        success: function (result) {
            if (result["data"]) {
                if (result['data'] == "") {
                    $('#no_data').show();
                } else {
                    $('#no_data').hide();
                }
                var tables = "<thead>";
                tables += "<tr>";
                tables += "<th>#</th>";
                tables += "<th>Name</th>";
                tables += "<th>Price</th>";
                tables += "<th>Make</th>";
                tables += "<th>Year</th>";
                tables += "<th>Image</th>";
                tables += "</tr>";
                tables += "</thead>";
                tables += "<tbody>";
                var j = 1;
                for (var i = 0; i < result["data"].length; i++) {
                    tables += "<tr class=\"gradeX\">";
                    tables += "<td>" + j + "</td>";
                    tables += "<td> " + result["data"][i]["name"] + "</td>";
                    tables += "<td> " + result["data"][i]["price"] + "</td>";
                    tables += "<td> " + result["data"][i]["make"] + "</td>";
                    tables += "<td> " + result["data"][i]["year"] + "</td>";
                    tables += "<td><img width='40' height='40' src='" + result["data"][i]["carImage"] + "'/> </td>";
                    ++j;
                    tables += "</tr>";
                }
                tables += "</tbody>";
                $('#example').html(tables);
            }
        },
        Error: function (a, b, c) {
            bootbox.alert(a + "," + b + "," + "c");
        }
    });

};