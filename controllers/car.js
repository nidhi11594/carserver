var async = require("async");
var DAO = require("../DAO");
var models = require('../models');
exports.fetchCarList = function (Criteria, callbackRoute) {
    var options = {lean: true};
    async.waterfall([
            function (callback) {
                DAO.getData(models.car, Criteria, {} , options, callback);
            },
            function (data, callback) {
                if (data == null || data.length == 0) {
                    var error = {
                        response: {
                            message: "car not found",
                            data: []
                        },
                        statusCode: 200
                    };
                    callback(error);
                }
                var response = {
                    message: "Action Complete",
                    data: data
                };
                var success = {response: response, statusCode: 200};
                callback(null, success)
            }
        ],
        function (error, success) {
            if (error) {
                callbackRoute(error);
            } else {
                callbackRoute(null, success);
            }
        })
};
