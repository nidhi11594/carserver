/*
 ----------------------------------------
 GET DATA
 ----------------------------------------
 */
exports.getData = function (model, query, projection, options, callback) {

    model.find(query, projection, options, function (err, data) {

        if (err) {
            var response = {
                message: "error in execution",
                data: {}
            };
            var errResponse = {
                response: response,
                details: err,
                statusCode: 500
            };

            callback(errResponse);
        }
        else {
            callback(null, data);
        }
    });
};